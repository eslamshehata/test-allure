describe('My First Test', () => {
  it('Gets, types and asserts', () => {
    cy.visit('https://example.cypress.io')

    cy.contains('type').click()

    // Should be on a new URL which
    // includes '/commands/actions'
    cy.url().should('include', '/commands/actions')

    // Get an input, type into it
    cy.get('.action-email').type('fake@email.com')

    //  Verify that the value has been updated
    cy.get('.action-email').should('have.value', 'fake@email.com')
  })
})




/*  describe('Actions', () => {
   beforeEach(() => {
     cy.visit('https://example.cypress.io/commands/actions');
   });

   it('.type() - type into a DOM element', () => {
    cy.allure().startStep('my step');
     cy.get('.action-email')
       .type('updated[email protected]')
       .should('have.value', 'updated[email protected]');
     cy.allure().endStep();

     cy.allure().startStep('my step 2');
     cy.get('.action-disabled')
      .type('disabled error checking', { force: true })
      .should('have.value', 'disabled error checking');
     cy.allure().endStep();

     cy.allure().startStep('my step 3');
     cy.get('.action-disabled').should('have.value', 'disabled error checking');
     cy.allure().endStep();

     cy.allure().startStep('4.Step four');
     cy.get('.action-email')
       .type('updated[email protected]')
       .should('have.value', 'updated[email protected]');
    cy.allure().endStep();

     cy.allure().startStep('5.Step five');
    cy.get('.action-disabled')
       .type('disabled error checking', { force: true });
     cy.allure().startStep('Expected result - Some expected result');
     cy.get('.action-disabled').should('have.value', 'disabled error checking');
     cy.allure().endStep();
   });
 }); */
